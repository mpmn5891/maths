# README #

Maths is a direct copy paste of [JOML](https://github.com/JOML-CI/JOML)

I removed all classes and methods dealing in non-float types and refactored class names, as the library only works with floats, the trailing 'f' was unnecessary.

Maths still uses the original MIT license.

Download the standalone JAR file [HERE](https://bitbucket.org/mpmn5891/maths/downloads)