/*
 * (C) Copyright 2014-2016 Sri Harsha Chilakapati

 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.

 */

package bmg.maths;

import java.util.ArrayList;
import java.util.Deque;
import java.util.LinkedList;
import java.util.List;

/**
 * Implemented Chilakapati's ReusableStack from <a href="https://github.com/sriharshachilakapati/SilenceEngine">SilenceEngine</a>
 * 
 * @author Sri Harsha Chilakapati
 * @author Matthew Newbury
 *
 */
public final class ReusableStack<T>
{
	private Deque<T> stack;
	private List<T> list;

	private ObjectProvider<T> op;

	public ReusableStack(ObjectProvider<T> op)
	{
		stack = new LinkedList<>();
		list = new ArrayList<>();
		this.op = op;
	}

	public T pop()
	{
		if (stack.size() == 0)
		{
			T object = op.createObject();
			list.add(object);
			stack.push(object);
		}
		return stack.pop();
	}

	public void push(T object)
	{
		stack.push(object);
	}

	public List<T> getAsList()
	{
		return list;
	}

	@FunctionalInterface
	public interface ObjectProvider<T>
	{
		T createObject();
	}
}
