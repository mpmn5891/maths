/*
 * (C) Copyright 2015-2016 Richard Greenlees

 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.

 */
package bmg.maths;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;
import java.nio.ByteBuffer;
import java.nio.FloatBuffer;
import java.text.DecimalFormat;
import java.text.NumberFormat;

/**
 * Represents a 2D vector with single-precision.
 *
 * @author RGreenlees
 * @author Kai Burjack
 * @author Matthew Newbury
 */
public class Vector2 implements Externalizable
{
	public static final ReusableStack<Vector2> REUSABLE_STACK = new ReusableStack<>(Vector2::new);

	public static final Vector2 ZERO = new Vector2(0, 0);
	public static final Vector2 AXIS_X = new Vector2(1, 0);
	public static final Vector2 AXIS_Y = new Vector2(0, 1);

	private static final long serialVersionUID = 1L;

	/**
	 * The x component of the vector.
	 */
	public float x;
	/**
	 * The y component of the vector.
	 */
	public float y;

	/**
	 * Create a new {@link Vector2} and initialize its components to zero.
	 */
	public Vector2()
	{
	}

	/**
	 * Create a new {@link Vector2} and initialize both of its components with
	 * the given value.
	 *
	 * @param d
	 *            the value of both components
	 */
	public Vector2(float d)
	{
		this(d, d);
	}

	/**
	 * Create a new {@link Vector2} and initialize its components to the given
	 * values.
	 * 
	 * @param x
	 *            the x component
	 * @param y
	 *            the y component
	 */
	public Vector2(float x, float y)
	{
		this.x = x;
		this.y = y;
	}

	/**
	 * Create a new {@link Vector2} and initialize its components to the one of
	 * the given vector.
	 * 
	 * @param v
	 *            the {@link Vector2} to copy the values from
	 */
	public Vector2(Vector2 v)
	{
		x = v.x;
		y = v.y;
	}

	/**
	 * Create a new {@link Vector2} and read this vector from the supplied
	 * {@link ByteBuffer} at the current buffer {@link ByteBuffer#position()
	 * position}.
	 * <p>
	 * This method will not increment the position of the given ByteBuffer.
	 * <p>
	 * In order to specify the offset into the ByteBuffer at which the vector is
	 * read, use {@link #Vector2(int, ByteBuffer)}, taking the absolute
	 * position as parameter.
	 *
	 * @param buffer
	 *            values will be read in <tt>x, y</tt> order
	 * @see #Vector2(int, ByteBuffer)
	 */
	public Vector2(ByteBuffer buffer)
	{
		this(buffer.position(), buffer);
	}

	/**
	 * Create a new {@link Vector2} and read this vector from the supplied
	 * {@link ByteBuffer} starting at the specified absolute buffer
	 * position/index.
	 * <p>
	 * This method will not increment the position of the given ByteBuffer.
	 *
	 * @param index
	 *            the absolute position into the ByteBuffer
	 * @param buffer
	 *            values will be read in <tt>x, y</tt> order
	 */
	public Vector2(int index, ByteBuffer buffer)
	{
		x = buffer.getFloat(index);
		y = buffer.getFloat(index + 4);
	}

	/**
	 * Create a new {@link Vector2} and read this vector from the supplied
	 * {@link FloatBuffer} at the current buffer {@link FloatBuffer#position()
	 * position}.
	 * <p>
	 * This method will not increment the position of the given FloatBuffer.
	 * <p>
	 * In order to specify the offset into the FloatBuffer at which the vector
	 * is read, use {@link #Vector2(int, FloatBuffer)}, taking the absolute
	 * position as parameter.
	 *
	 * @param buffer
	 *            values will be read in <tt>x, y</tt> order
	 * @see #Vector2(int, FloatBuffer)
	 */
	public Vector2(FloatBuffer buffer)
	{
		this(buffer.position(), buffer);
	}

	/**
	 * Create a new {@link Vector2} and read this vector from the supplied
	 * {@link FloatBuffer} starting at the specified absolute buffer
	 * position/index.
	 * <p>
	 * This method will not increment the position of the given FloatBuffer.
	 *
	 * @param index
	 *            the absolute position into the FloatBuffer
	 * @param buffer
	 *            values will be read in <tt>x, y</tt> order
	 */
	public Vector2(int index, FloatBuffer buffer)
	{
		x = buffer.get(index);
		y = buffer.get(index + 1);
	}

	/**
	 * Set the x and y components to the supplied value.
	 *
	 * @param d
	 *            the value of both components
	 * @return this
	 */
	public Vector2 set(float d)
	{
		return set(d, d);
	}

	/**
	 * Set the x and y components to the supplied values.
	 * 
	 * @param x
	 *            the x component
	 * @param y
	 *            the y component
	 * @return this
	 */
	public Vector2 set(float x, float y)
	{
		this.x = x;
		this.y = y;
		return this;
	}

	/**
	 * Set this {@link Vector2} to the values of v.
	 * 
	 * @param v
	 *            the vector to copy from
	 * @return this
	 */
	public Vector2 set(Vector2 v)
	{
		x = v.x;
		y = v.y;
		return this;
	}

	/**
	 * Read this vector from the supplied {@link ByteBuffer} at the current
	 * buffer {@link ByteBuffer#position() position}.
	 * <p>
	 * This method will not increment the position of the given ByteBuffer.
	 * <p>
	 * In order to specify the offset into the ByteBuffer at which the vector is
	 * read, use {@link #set(int, ByteBuffer)}, taking the absolute position as
	 * parameter.
	 *
	 * @param buffer
	 *            values will be read in <tt>x, y</tt> order
	 * @return this
	 * @see #set(int, ByteBuffer)
	 */
	public Vector2 set(ByteBuffer buffer)
	{
		return set(buffer.position(), buffer);
	}

	/**
	 * Read this vector from the supplied {@link ByteBuffer} starting at the
	 * specified absolute buffer position/index.
	 * <p>
	 * This method will not increment the position of the given ByteBuffer.
	 *
	 * @param index
	 *            the absolute position into the ByteBuffer
	 * @param buffer
	 *            values will be read in <tt>x, y</tt> order
	 * @return this
	 */
	public Vector2 set(int index, ByteBuffer buffer)
	{
		x = buffer.getFloat(index);
		y = buffer.getFloat(index + 4);
		return this;
	}

	/**
	 * Read this vector from the supplied {@link FloatBuffer} at the current
	 * buffer {@link FloatBuffer#position() position}.
	 * <p>
	 * This method will not increment the position of the given FloatBuffer.
	 * <p>
	 * In order to specify the offset into the FloatBuffer at which the vector
	 * is read, use {@link #set(int, FloatBuffer)}, taking the absolute position
	 * as parameter.
	 *
	 * @param buffer
	 *            values will be read in <tt>x, y</tt> order
	 * @return this
	 * @see #set(int, FloatBuffer)
	 */
	public Vector2 set(FloatBuffer buffer)
	{
		return set(buffer.position(), buffer);
	}

	/**
	 * Read this vector from the supplied {@link FloatBuffer} starting at the
	 * specified absolute buffer position/index.
	 * <p>
	 * This method will not increment the position of the given FloatBuffer.
	 *
	 * @param index
	 *            the absolute position into the FloatBuffer
	 * @param buffer
	 *            values will be read in <tt>x, y</tt> order
	 * @return this
	 */
	public Vector2 set(int index, FloatBuffer buffer)
	{
		x = buffer.get(index);
		y = buffer.get(index + 1);
		return this;
	}

	/**
	 * Store this vector into the supplied {@link ByteBuffer} at the current
	 * buffer {@link ByteBuffer#position() position}.
	 * <p>
	 * This method will not increment the position of the given ByteBuffer.
	 * <p>
	 * In order to specify the offset into the ByteBuffer at which the vector is
	 * stored, use {@link #get(int, ByteBuffer)}, taking the absolute position
	 * as parameter.
	 *
	 * @param buffer
	 *            will receive the values of this vector in <tt>x, y</tt> order
	 * @return the passed in buffer
	 * @see #get(int, ByteBuffer)
	 */
	public ByteBuffer get(ByteBuffer buffer)
	{
		return get(buffer.position(), buffer);
	}

	/**
	 * Store this vector into the supplied {@link ByteBuffer} starting at the
	 * specified absolute buffer position/index.
	 * <p>
	 * This method will not increment the position of the given ByteBuffer.
	 *
	 * @param index
	 *            the absolute position into the ByteBuffer
	 * @param buffer
	 *            will receive the values of this vector in <tt>x, y</tt> order
	 * @return the passed in buffer
	 */
	public ByteBuffer get(int index, ByteBuffer buffer)
	{
		buffer.putFloat(index, x);
		buffer.putFloat(index + 4, y);
		return buffer;
	}

	/**
	 * Store this vector into the supplied {@link FloatBuffer} at the current
	 * buffer {@link FloatBuffer#position() position}.
	 * <p>
	 * This method will not increment the position of the given FloatBuffer.
	 * <p>
	 * In order to specify the offset into the FloatBuffer at which the vector
	 * is stored, use {@link #get(int, FloatBuffer)}, taking the absolute
	 * position as parameter.
	 *
	 * @param buffer
	 *            will receive the values of this vector in <tt>x, y</tt> order
	 * @return the passed in buffer
	 * @see #get(int, FloatBuffer)
	 */
	public FloatBuffer get(FloatBuffer buffer)
	{
		return get(buffer.position(), buffer);
	}

	/**
	 * Store this vector into the supplied {@link FloatBuffer} starting at the
	 * specified absolute buffer position/index.
	 * <p>
	 * This method will not increment the position of the given FloatBuffer.
	 *
	 * @param index
	 *            the absolute position into the FloatBuffer
	 * @param buffer
	 *            will receive the values of this vector in <tt>x, y</tt> order
	 * @return the passed in buffer
	 */
	public FloatBuffer get(int index, FloatBuffer buffer)
	{
		buffer.put(index, x);
		buffer.put(index + 1, y);
		return buffer;
	}

	public Vector2 copy()
	{
		return new Vector2(this);
	}

	/**
	 * Set this vector to be one of its perpendicular vectors.
	 * 
	 * @return this
	 */
	public Vector2 perpendicular()
	{
		return set(y, x * -1);
	}

	/**
	 * Subtract <code>v</code> from this vector.
	 * 
	 * @param v
	 *            the vector to subtract
	 * @return this
	 */
	public Vector2 sub(Vector2 v)
	{
		x -= v.x;
		y -= v.y;
		return this;
	}

	/**
	 * Subtract <code>v</code> from <code>this</code> vector and store the
	 * result in <code>dest</code>.
	 * 
	 * @param v
	 *            the vector to subtract
	 * @param dest
	 *            will hold the result
	 * @return dest
	 */
	public Vector2 sub(Vector2 v, Vector2 dest)
	{
		dest.x = x - v.x;
		dest.y = y - v.y;
		return dest;
	}

	/**
	 * Subtract <tt>(x, y)</tt> from this vector.
	 * 
	 * @param x
	 *            the x component to subtract
	 * @param y
	 *            the y component to subtract
	 * @return this
	 */
	public Vector2 sub(float x, float y)
	{
		this.x -= x;
		this.y -= y;
		return this;
	}

	/**
	 * Subtract <tt>(x, y)</tt> from this vector and store the result in
	 * <code>dest</code>.
	 * 
	 * @param x
	 *            the x component to subtract
	 * @param y
	 *            the y component to subtract
	 * @param dest
	 *            will hold the result
	 * @return dest
	 */
	public Vector2 sub(float x, float y, Vector2 dest)
	{
		dest.x = this.x - x;
		dest.y = this.y - y;
		return dest;
	}

	/**
	 * Return the dot product of this vector and <code>v</code>.
	 * 
	 * @param v
	 *            the other vector
	 * @return the dot product
	 */
	public float dot(Vector2 v)
	{
		return x * v.x + y * v.y;
	}

	public float angle()
	{
		return MathUtils.atan2(y, x);
	}

	/**
	 * Return the angle between this vector and the supplied vector.
	 * 
	 * @param v
	 *            the other vector
	 * @return the angle, in radians
	 */
	public float angle(Vector2 v)
	{
		float dot = x * v.x + y * v.y;
		float det = x * v.y - y * v.x;
		return (float) Math.atan2(det, dot);
	}

	/**
	 * Rotate this vector by angle
	 * 
	 * @param angle
	 *            the angle to rotate by
	 * @return this
	 */
	public Vector2 rotate(float angle)
	{
		float cos = MathUtils.cos(angle);
		float sin = MathUtils.sin(angle);
		return set(x * cos - y * sin, x * sin + y * cos);
	}

	/**
	 * Rotate this vector by angle and store the result in dest
	 * 
	 * @param angle
	 *            the angle to rotate by
	 * @param dest
	 *            will hold the result
	 * @return dest
	 */
	public Vector2 rotate(float angle, Vector2 dest)
	{
		float cos = MathUtils.cos(angle);
		float sin = MathUtils.sin(angle);
		return dest.set(x * cos - y * sin, x * sin + y * cos);
	}

	/**
	 * Return the length of this vector.
	 * 
	 * @return the length
	 */
	public float length()
	{
		return (float) Math.sqrt((x * x) + (y * y));
	}

	/**
	 * Return the length squared of this vector.
	 * 
	 * @return the length squared
	 */
	public float lengthSquared()
	{
		return x * x + y * y;
	}

	/**
	 * Return the distance between this and <code>v</code>.
	 * 
	 * @param v
	 *            the other vector
	 * @return the distance
	 */
	public float distance(Vector2 v)
	{
		float dx = v.x - x;
		float dy = v.y - y;
		return (float) Math.sqrt(dx * dx + dy * dy);
	}

	/**
	 * Normalize this vector.
	 * 
	 * @return this
	 */
	public Vector2 normalize()
	{
		float invLength = (float) (1.0 / Math.sqrt(x * x + y * y));
		x *= invLength;
		y *= invLength;
		return this;
	}

	/**
	 * Normalize this vector and store the result in <code>dest</code>.
	 * 
	 * @param dest
	 *            will hold the result
	 * @return dest
	 */
	public Vector2 normalize(Vector2 dest)
	{
		float invLength = (float) (1.0 / Math.sqrt(x * x + y * y));
		dest.x = x * invLength;
		dest.y = y * invLength;
		return dest;
	}

	/**
	 * Add <code>v</code> to this vector.
	 * 
	 * @param v
	 *            the vector to add
	 * @return this
	 */
	public Vector2 add(Vector2 v)
	{
		x += v.x;
		y += v.y;
		return this;
	}

	/**
	 * Add the supplied vector to this one and store the result in
	 * <code>dest</code>.
	 *
	 * @param v
	 *            the vector to add
	 * @param dest
	 *            will hold the result
	 * @return dest
	 */
	public Vector2 add(Vector2 v, Vector2 dest)
	{
		dest.x = x + v.x;
		dest.y = y + v.y;
		return dest;
	}

	/**
	 * Increment the components of this vector by the given values.
	 * 
	 * @param x
	 *            the x component to add
	 * @param y
	 *            the y component to add
	 * @return this
	 */
	public Vector2 add(float x, float y)
	{
		this.x += x;
		this.y += y;
		return this;
	}

	/**
	 * Increment the components of this vector by the given values and store the
	 * result in <code>dest</code>.
	 * 
	 * @param x
	 *            the x component to add
	 * @param y
	 *            the y component to add
	 * @param dest
	 *            will hold the result
	 * @return dest
	 */
	public Vector2 add(float x, float y, Vector2 dest)
	{
		dest.x = this.x + x;
		dest.y = this.y + y;
		return dest;
	}

	/**
	 * Set all components to zero.
	 * 
	 * @return this
	 */
	public Vector2 zero()
	{
		this.x = 0.0f;
		this.y = 0.0f;
		return this;
	}

	public void writeExternal(ObjectOutput out) throws IOException
	{
		out.writeFloat(x);
		out.writeFloat(y);
	}

	public void readExternal(ObjectInput in) throws IOException,
			ClassNotFoundException
	{
		x = in.readFloat();
		y = in.readFloat();
	}

	/**
	 * Negate this vector.
	 * 
	 * @return this
	 */
	public Vector2 negate()
	{
		x = -x;
		y = -y;
		return this;
	}

	/**
	 * Negate this vector and store the result in <code>dest</code>.
	 * 
	 * @param dest
	 *            will hold the result
	 * @return dest
	 */
	public Vector2 negate(Vector2 dest)
	{
		dest.x = -x;
		dest.y = -y;
		return dest;
	}

	/**
	 * Multiply the components of this vector by the given scalar.
	 * 
	 * @param scalar
	 *            the value to multiply this vector's components by
	 * @return this
	 */
	public Vector2 mul(float scalar)
	{
		this.x *= scalar;
		this.y *= scalar;
		return this;
	}

	/**
	 * Multiply the components of this vector by the given scalar and store the
	 * result in <code>dest</code>.
	 * 
	 * @param scalar
	 *            the value to multiply this vector's components by
	 * @param dest
	 *            will hold the result
	 * @return dest
	 */
	public Vector2 mul(float scalar, Vector2 dest)
	{
		dest.x = x * scalar;
		dest.y = y * scalar;
		return dest;
	}

	/**
	 * Multiply the components of this Vector2 by the given scalar values and
	 * store the result in <code>this</code>.
	 * 
	 * @param x
	 *            the x component to multiply this vector by
	 * @param y
	 *            the y component to multiply this vector by
	 * @return this
	 */
	public Vector2 mul(float x, float y)
	{
		this.x *= x;
		this.y *= y;
		return this;
	}

	/**
	 * Multiply the components of this Vector2 by the given scalar values and
	 * store the result in <code>dest</code>.
	 * 
	 * @param x
	 *            the x component to multiply this vector by
	 * @param y
	 *            the y component to multiply this vector by
	 * @param dest
	 *            will hold the result
	 * @return dest
	 */
	public Vector2 mul(float x, float y, Vector2 dest)
	{
		dest.x = this.x * x;
		dest.y = this.y * y;
		return dest;
	}

	/**
	 * Multiply this Vector2 component-wise by another Vector2.
	 * 
	 * @param v
	 *            the vector to multiply by
	 * @return this
	 */
	public Vector2 mul(Vector2 v)
	{
		x *= v.x;
		y *= v.y;
		return this;
	}

	/**
	 * Multiply this Vector2 component-wise by another Vector2 and store the
	 * result in <code>dest</code>.
	 * 
	 * @param v
	 *            the vector to multiply by
	 * @param dest
	 *            will hold the result
	 * @return dest
	 */
	public Vector2 mul(Vector2 v, Vector2 dest)
	{
		dest.x = x * v.x;
		dest.y = y * v.y;
		return dest;
	}

	/**
	 * Linearly interpolate <code>this</code> and <code>other</code> using the
	 * given interpolation factor <code>t</code> and store the result in
	 * <code>this</code>.
	 * <p>
	 * If <code>t</code> is <tt>0.0</tt> then the result is <code>this</code>.
	 * If the interpolation factor is <code>1.0</code> then the result is
	 * <code>other</code>.
	 * 
	 * @param other
	 *            the other vector
	 * @param t
	 *            the interpolation factor between 0.0 and 1.0
	 * @return this
	 */
	public Vector2 lerp(Vector2 other, float t)
	{
		return lerp(other, t, this);
	}

	/**
	 * Linearly interpolate <code>this</code> and <code>other</code> using the
	 * given interpolation factor <code>t</code> and store the result in
	 * <code>dest</code>.
	 * <p>
	 * If <code>t</code> is <tt>0.0</tt> then the result is <code>this</code>.
	 * If the interpolation factor is <code>1.0</code> then the result is
	 * <code>other</code>.
	 * 
	 * @param other
	 *            the other vector
	 * @param t
	 *            the interpolation factor between 0.0 and 1.0
	 * @param dest
	 *            will hold the result
	 * @return dest
	 */
	public Vector2 lerp(Vector2 other, float t, Vector2 dest)
	{
		dest.x = x + (other.x - x) * t;
		dest.y = y + (other.y - y) * t;
		return dest;
	}

	public int hashCode()
	{
		final int prime = 31;
		int result = 1;
		result = prime * result + Float.floatToIntBits(x);
		result = prime * result + Float.floatToIntBits(y);
		return result;
	}

	public boolean equals(Object obj)
	{
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Vector2 other = (Vector2) obj;
		if (Float.floatToIntBits(x) != Float.floatToIntBits(other.x))
			return false;
		if (Float.floatToIntBits(y) != Float.floatToIntBits(other.y))
			return false;
		return true;
	}

	/**
	 * Return a string representation of this vector.
	 * <p>
	 * This method creates a new {@link DecimalFormat} on every invocation with
	 * the format string "<tt> 0.000E0;-</tt>".
	 * 
	 * @return the string representation
	 */
	public String toString()
	{
		DecimalFormat formatter = new DecimalFormat(" 0.000E0;-"); //$NON-NLS-1$
		return toString(formatter).replaceAll("E(\\d+)", "E+$1"); //$NON-NLS-1$ //$NON-NLS-2$
	}

	/**
	 * Return a string representation of this vector by formatting the vector
	 * components with the given {@link NumberFormat}.
	 * 
	 * @param formatter
	 *            the {@link NumberFormat} used to format the vector components
	 *            with
	 * @return the string representation
	 */
	public String toString(NumberFormat formatter)
	{
		return "(" + formatter.format(x) + " " + formatter.format(y) + ")"; //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
	}

	/**
	 * Add the component-wise multiplication of <code>a * b</code> to this
	 * vector.
	 * 
	 * @param a
	 *            the first multiplicand
	 * @param b
	 *            the second multiplicand
	 * @return this
	 */
	public Vector2 fma(Vector2 a, Vector2 b)
	{
		x += a.x * b.x;
		y += a.y * b.y;
		return this;
	}

	/**
	 * Add the component-wise multiplication of <code>a * b</code> to this
	 * vector.
	 * 
	 * @param a
	 *            the first multiplicand
	 * @param b
	 *            the second multiplicand
	 * @return this
	 */
	public Vector2 fma(float a, Vector2 b)
	{
		x += a * b.x;
		y += a * b.y;
		return this;
	}

	/**
	 * Add the component-wise multiplication of <code>a * b</code> to this
	 * vector and store the result in <code>dest</code>.
	 * 
	 * @param a
	 *            the first multiplicand
	 * @param b
	 *            the second multiplicand
	 * @param dest
	 *            will hold the result
	 * @return dest
	 */
	public Vector2 fma(Vector2 a, Vector2 b, Vector2 dest)
	{
		dest.x = x + a.x * b.x;
		dest.y = y + a.y * b.y;
		return dest;
	}

	/**
	 * Add the component-wise multiplication of <code>a * b</code> to this
	 * vector and store the result in <code>dest</code>.
	 * 
	 * @param a
	 *            the first multiplicand
	 * @param b
	 *            the second multiplicand
	 * @param dest
	 *            will hold the result
	 * @return dest
	 */
	public Vector2 fma(float a, Vector2 b, Vector2 dest)
	{
		dest.x = x + a * b.x;
		dest.y = y + a * b.y;
		return dest;
	}

}
